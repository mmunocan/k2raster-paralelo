#! /usr/bin/env bash

g++ -c libs/z_order.cpp -O3 -march=native
mv z_order.o bin/z_order.o
nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-secuencial.cu -w -std=c++14 -O3 bin/*.o -o encode-secuencial
nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-paralelo.cu -w -std=c++14 -O3 bin/*.o -o encode-paralelo
nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-lecturaz.cu -w -std=c++14 -O3 bin/*.o -o encode-lecturaz
nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-pinned.cu -w -std=c++14 -O3 bin/*.o -o encode-pinned

nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-secuencial-tiempo.cu -w -std=c++14 -O3 bin/*.o -o encode-secuencial-tiempo
nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-paralelo-tiempo.cu -w -std=c++14 -O3 bin/*.o -o encode-paralelo-tiempo
nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-lecturaz-tiempo.cu -w -std=c++14 -O3 bin/*.o -o encode-lecturaz-tiempo
nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-pinned-tiempo.cu -w -std=c++14 -O3 bin/*.o -o encode-pinned-tiempo

nvcc -Xcompiler -fopenmp -arch=sm_61 -rdc=true encode-paralelo-tiempo-kernel.cu -w -std=c++14 -O3 bin/*.o -o encode-paralelo-tiempo-kernel

