#include <iostream>
#include <fstream>
#include <chrono>
#include <cmath>
#include <climits>

#include <thrust/scan.h>
#include <thrust/execution_policy.h>

#include "libs/z_order.hpp"

using namespace std;

unsigned int get_lado(unsigned int filas, unsigned int columnas){
	if(filas >= columnas){
		return  (1 << 32-__builtin_clz(filas-1));
	}else{
		return  (1 << 32-__builtin_clz(columnas-1));
	}
}

unsigned long get_tam_piramide(unsigned int level){
	unsigned long tam = 1;
	unsigned long tam_total = 0;
	for(unsigned int l = 0; l < level; l++){
		tam_total += tam;
		tam = tam << 2;
	}
	return tam_total;
}

void print_array(const short * arr, unsigned long n){
	for(unsigned long i = 0; i < n; i++){
		cout << arr[i] << ",";
	}
	cout << endl;
}

void print_array(const bool * arr, unsigned long n){
	for(unsigned long i = 0; i < n; i++){
		cout << arr[i] << ",";
	}
	cout << endl;
}

void read_dataset(char * filename, short * rowMajor, unsigned int filas, unsigned int columnas, unsigned int lado, int * minimo, int * maximo){
	ifstream fe(filename);
	if(!fe.is_open()){
		printf("Error al abrir el archivo %s\n", filename);
		return;
	}
	if(filas > lado || columnas > lado){
		printf("Los valores filas, columnas y lado son incorrectos\n");
		return;
	}
	
	int * temporal = new int[lado*lado];
	*minimo = INT_MAX;
	*maximo = INT_MIN;
	int valor;
	unsigned long i;
	for(unsigned int p = 0; p < lado; p++){
		for(unsigned int q = 0; q < lado; q++){
			i = p * lado + q;
			if(p < filas && q < columnas){
				fe.read((char *) (& valor), sizeof(int));
				temporal[i] = valor;
				if(valor < *minimo) *minimo = valor;
				if(valor > *maximo) *maximo = valor;
			}else{
				temporal[i] = -1;	// Extension
			}
			
		}
	}
	fe.close();
	
	for(i = 0; i < lado*lado; i++){
		if(temporal[i] != -1){
			rowMajor[i] = temporal[i]-*minimo;
		}else{
			rowMajor[i] = -1;
		}
	}
	delete [] temporal;
	
}

__device__ uint64_t interleave_uint32_with_zeros(uint32_t input)  {
    uint64_t word = input;
    word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
    word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
    word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
    word = (word ^ (word << 2 )) & 0x3333333333333333;
    word = (word ^ (word << 1 )) & 0x5555555555555555;
	
    return word;
}

__device__ unsigned long d_get_zorder(unsigned int p, unsigned int q){
	return interleave_uint32_with_zeros(q) | (interleave_uint32_with_zeros(p) << 1);
}

__global__ void get_zorder(const short * d_rowMajor, short * d_zorder, unsigned int lado, unsigned long cant_elementos){
	unsigned long id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < cant_elementos){
		unsigned int p = (unsigned int) (id / lado);
		unsigned int q = (unsigned int) (id % lado);
		d_zorder[d_get_zorder(p, q)] = d_rowMajor[id];
	}
}

__global__ void llenar_primer_nivel(const short * d_zorder, short * d_min, short * d_max, bool * d_filter, unsigned long cant_elementos){
	unsigned long id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned long cant_cuarta = cant_elementos >> 2;
	unsigned long base_zorder, pos_l_max;
	unsigned char i;
	
	if(id < cant_cuarta){
		base_zorder = id << 2;
		
		short min = d_zorder[base_zorder];
		short max = d_zorder[base_zorder];

		for(i = 1; i < 4; i++){
			if(d_zorder[base_zorder+i] < min){
				min = d_zorder[base_zorder+i];
			}
			if(d_zorder[base_zorder+i] > max){
				max = d_zorder[base_zorder+i];
			}
		}

		d_min[id] = min;
		d_max[id] = max;
		
		
		if(min == max){
			d_filter[id] = false;
		}else{
			d_filter[id] = true;
		}
		
	}
}


__global__ void niveles_superiores(short * d_min, short * d_max, bool * d_filter, unsigned long cant_elementos, unsigned int level){
	unsigned long id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned long cant_cuarta = cant_elementos >> 2; 
	unsigned long base_anterior = 0; 
	unsigned long base = cant_elementos; 
	unsigned long base_zorder, pos_l_min, pos_l_max;
	unsigned char i;
	
	for(unsigned int l = 1; l < level; l++){
		if(id < cant_cuarta){
			base_zorder = base_anterior + (id << 2);
			
			short min = d_min[base_zorder];
			short max = d_max[base_zorder];

			for(i = 1; i < 4; i++){
				if(d_min[base_zorder+i] < min){
					min = d_min[base_zorder+i];
				}
				if(d_max[base_zorder+i] > max){
					max = d_max[base_zorder+i];
				}
			}

			d_min[base+id] = min;
			d_max[base+id] = max;

			if(min == max){
				d_filter[base+id] = false;
			}else{
				d_filter[base+id] = true;
			}
			
		}
		
		base_anterior = base; 
		base += cant_cuarta; 
		cant_elementos = cant_cuarta; 
		cant_cuarta = cant_cuarta>>2; 
		
		__syncthreads();
		
	}	
}

__global__ void construir_k2_raster(const short * d_zorder, const short * d_min, const short * d_max, const bool * d_filter, const long * d_count, short * d_l_min, short * d_l_max, bool * d_l_filter, unsigned long cant_elementos, unsigned int level){
	unsigned long id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned long cant_cuarta = cant_elementos >> 2;
	unsigned long base_zorder, pos_l_min, pos_l_max, pos_l_filter;
	unsigned char i;
	
	/*** Primer nivel ***/
	if(id < cant_cuarta){
		base_zorder = id << 2;
		
		if(d_filter[id]){
			pos_l_max = d_count[id] << 2;
			for(i = 0; i < 4; i++){
				d_l_max[pos_l_max+i] = d_max[id]-d_zorder[base_zorder+i];
			}
			
		}
	}
	
	cant_elementos = cant_cuarta;
	cant_cuarta = cant_cuarta>>2;
	unsigned long base_anterior = 0; 
	unsigned long base = cant_elementos;
	unsigned long base_l1 = base;
	
	/*** Niveles superiores ***/
	for(unsigned int l = 1; l < level; l++){
		__syncthreads();
		if(id < cant_cuarta){
			base_zorder = base_anterior + (id << 2);
			if(d_filter[base+id]){
				/*** Completación de l_max y l_filter ***/
				pos_l_max = d_count[base+id] << 2;
				pos_l_filter = (d_count[base+id]-d_count[base_l1]) << 2;
				for(i = 0; i < 4; i++){
					d_l_max[pos_l_max+i] = d_max[base+id]-d_max[base_zorder+i];
					d_l_filter[pos_l_filter+i] = d_filter[base_zorder+i];
				}
				
			}
			/*** Completación de l_min ***/
			for(i = 0; i < 4; i++){
				if(d_filter[base_zorder+i]){
					pos_l_min = d_count[base_zorder+i];
					d_l_min[pos_l_min] = d_min[base_zorder+i]-d_min[base+id];
				}
			}
		}
		base_anterior = base;
		base += cant_cuarta;
		cant_elementos = cant_cuarta;
		cant_cuarta = cant_cuarta>>2; 
		
		
	}
	
}

void get_k2raster(const short * rowMajor, short * l_max, short * l_min, bool * l_filter, unsigned long cant_elementos, unsigned long tam, unsigned long level, unsigned int lado, unsigned int hpb){
	unsigned long size = cant_elementos * sizeof(short);
	unsigned long min_max_size = tam * sizeof(short);
	unsigned long filter_size = tam * sizeof(bool);
	unsigned long l_min_size = tam * sizeof(short);
	unsigned long l_max_size = (tam+cant_elementos) * sizeof(short);
	unsigned long count_size = tam * sizeof(long);
	
	unsigned long cant_cuarta = cant_elementos >> 2;
	
	short *d_rowMajor, *d_zorder, *d_min, *d_max, *d_l_min, *d_l_max;
	long *d_count;
	bool *d_filter, *d_l_filter;
	
	cudaMalloc((void**)&d_rowMajor, size);
	cudaMalloc((void**)&d_zorder, size);
	cudaMalloc((void**)&d_min, min_max_size);
	cudaMalloc((void**)&d_max, min_max_size);
	cudaMalloc((void**)&d_filter, filter_size);
	cudaMalloc((void**)&d_l_min, l_min_size);
	cudaMalloc((void**)&d_l_max, l_max_size);
	cudaMalloc((void**)&d_l_filter, filter_size);
	cudaMalloc((void**)&d_count, count_size);
	cudaMemcpy(d_rowMajor, rowMajor, size, cudaMemcpyHostToDevice);
	
	/*** Obtener z-order ***/
	unsigned int bloques_por_grilla = cant_elementos > hpb ? ceil(cant_elementos / hpb) : 1;
	unsigned int hilos_por_bloque = cant_elementos > hpb ? hpb : cant_elementos;
	get_zorder<<<bloques_por_grilla, hilos_por_bloque>>>(d_rowMajor, d_zorder, lado, cant_elementos);
	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess){
		cout<<"Error get_zorder: "<<cudaGetErrorString(err)<<endl;
	}
	cudaDeviceSynchronize();
	
	/*** Primer nivel ***/
	bloques_por_grilla = cant_cuarta > hpb ? ceil(cant_cuarta/hpb)+1 : 1;
	hilos_por_bloque = cant_cuarta > hpb ? hpb : cant_cuarta;
	llenar_primer_nivel<<<bloques_por_grilla, hilos_por_bloque>>>(d_zorder, d_min, d_max, d_filter, cant_elementos);
	err = cudaGetLastError();
	if (err != cudaSuccess){
		cout<<"Error llenar_primer_nivel: "<<cudaGetErrorString(err)<<endl;
	}
	cudaDeviceSynchronize();
	
	/*** Niveles superiores ***/
	niveles_superiores<<<bloques_por_grilla, hilos_por_bloque>>>(d_min, d_max, d_filter, cant_cuarta, level);
	err = cudaGetLastError();
	if (err != cudaSuccess){
		cout<<"Error niveles_superiores: "<<cudaGetErrorString(err)<<endl;
	}
	cudaDeviceSynchronize();
	
	thrust::exclusive_scan(thrust::device, d_filter, d_filter+tam, d_count);
	
	/*** Obtener elementos k2raster ***/
	construir_k2_raster<<<bloques_por_grilla, hilos_por_bloque>>>(d_zorder, d_min, d_max, d_filter, d_count, d_l_min, d_l_max, d_l_filter, cant_elementos, level);
	err = cudaGetLastError();
	if (err != cudaSuccess){
		cout<<"Error construir_k2_raster: "<<cudaGetErrorString(err)<<endl;
	}
	cudaDeviceSynchronize();
	
	
	cudaMemcpy(l_min, d_l_min, l_min_size, cudaMemcpyDeviceToHost);
	cudaMemcpy(l_max, d_l_max, l_max_size, cudaMemcpyDeviceToHost);
	cudaMemcpy(l_filter, d_l_filter, filter_size, cudaMemcpyDeviceToHost);
	
	cudaFree(d_rowMajor);
	cudaFree(d_zorder);
	cudaFree(d_min);
	cudaFree(d_max);
	cudaFree(d_filter);
	cudaFree(d_l_min);
	cudaFree(d_l_max);
	cudaFree(d_count);
	cudaFree(d_l_filter);
	
	
}

int main(int argc, char ** argv){
	if(argc < 5){
		printf("%s <FILE> <ROWS> <COLS> <HPB>\n", argv[0]);
		return -1;
	}
	
	char * filename = argv[1];
	unsigned int filas = atoi(argv[2]);
	unsigned int columnas = atoi(argv[3]);
	unsigned int hpb = atoi(argv[4]);
	
	auto start = chrono::high_resolution_clock::now();
	auto finish = chrono::high_resolution_clock::now();
	long tiempo;
	
	unsigned int lado  = get_lado(filas, columnas);
	unsigned long cant_elementos = lado * lado;
	unsigned int level = ceil(log2(lado));
	unsigned long tam_total = get_tam_piramide(level);
	
	cout << "Raster tamaño " << filas << "X" << columnas << endl;
	cout << "Extendido a " << lado << "X" << lado << endl;
	cout << "Cantidad elementos: " << cant_elementos << endl;
	cout << "Niveles: " << level << endl;
	cout << "Tamaño piramide: " << tam_total << endl;
	
	unsigned long size = cant_elementos * sizeof(short);
	short * rowMajor;
	cudaHostAlloc((void**)&rowMajor, size, cudaHostAllocDefault);
	int * minimo = new int;
	int * maximo = new int;
	start = chrono::high_resolution_clock::now();
	read_dataset(filename, rowMajor, filas, columnas, lado, minimo, maximo);
	finish = chrono::high_resolution_clock::now();
	tiempo = chrono::duration_cast<chrono::milliseconds> (finish - start).count();
	
	cout << "Leí el raster en " << tiempo << " ms" << endl;
	
	if(cant_elementos <= 100){
		cout << "Array leido en row major order: " << endl;
		print_array(rowMajor, cant_elementos);
	}
	
	short *l_min, *l_max;
	bool * l_filter;
	size = tam_total * sizeof(short);
	cudaHostAlloc((void**)&l_min, size, cudaHostAllocDefault);
	size = (cant_elementos+tam_total) * sizeof(short);
	cudaHostAlloc((void**)&l_max, size, cudaHostAllocDefault);
	size = tam_total + sizeof(bool);
	cudaHostAlloc((void**)&l_filter, size, cudaHostAllocDefault);
	
	start = chrono::high_resolution_clock::now();
	get_k2raster(rowMajor, l_max, l_min, l_filter, cant_elementos, tam_total, level, lado, hpb);
	finish = chrono::high_resolution_clock::now();
	tiempo = chrono::duration_cast<chrono::milliseconds> (finish - start).count();
	
	cout << "Construi el k2raster en " << tiempo << " ms" << endl;
	cout << "Minimo: " << *minimo << endl;
	cout << "Maximo: " << *maximo << endl;
	if(tam_total <= 100){
		
		cout << "Array l_max: " << endl;
		print_array(l_max, cant_elementos+tam_total);
		cout << "Array l_min: " << endl;
		print_array(l_min, tam_total);
		cout << "Array l_filter: " << endl;
		print_array(l_filter, tam_total);
		
	}
	
	delete minimo;
	delete maximo;
	cudaFreeHost(rowMajor);
	cudaFreeHost(l_min);
	cudaFreeHost(l_max);
	cudaFreeHost(l_filter);
	
	return 0;
}