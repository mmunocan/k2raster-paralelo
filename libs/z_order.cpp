#include <immintrin.h>
#include "z_order.hpp"

/* FUNCION ALTERNATIVA (USAR EN CASO DE INCOMPATIBILIDAD)
unsigned long interleave_uint32_with_zeros(unsigned int input)  {
    unsigned long word = input;
    word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
    word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
    word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
    word = (word ^ (word << 2 )) & 0x3333333333333333;
    word = (word ^ (word << 1 )) & 0x5555555555555555;
    return word;
}
*/

unsigned long get_zorder(unsigned int p, unsigned int q){
	return _pdep_u64(q, 0x5555555555555555) | _pdep_u64(p, 0xaaaaaaaaaaaaaaaa);
	//return interleave_uint32_with_zeros(q) | (interleave_uint32_with_zeros(p) << 1);
}