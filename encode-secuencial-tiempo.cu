#include <iostream>
#include <fstream>
#include <chrono>
#include <cmath>
#include <climits>

#include "libs/z_order.hpp"

using namespace std;

unsigned int get_lado(unsigned int filas, unsigned int columnas){
	if(filas >= columnas){
		return  (1 << 32-__builtin_clz(filas-1));
	}else{
		return  (1 << 32-__builtin_clz(columnas-1));
	}
}

unsigned long get_tam_piramide(unsigned int level){
	unsigned long tam = 1;
	unsigned long tam_total = 0;
	for(unsigned int l = 0; l < level; l++){
		tam_total += tam;
		tam = tam << 2;
	}
	return tam_total;
}

void print_array(const short * arr, unsigned long n){
	for(unsigned long i = 0; i < n; i++){
		cout << arr[i] << ",";
	}
	cout << endl;
}

void print_array(const bool * arr, unsigned long n){
	for(unsigned long i = 0; i < n; i++){
		cout << arr[i] << ",";
	}
	cout << endl;
}

void read_dataset(char * filename, short * rowMajor, unsigned int filas, unsigned int columnas, unsigned int lado, int * minimo, int * maximo){
	ifstream fe(filename);
	if(!fe.is_open()){
		printf("Error al abrir el archivo %s\n", filename);
		return;
	}
	if(filas > lado || columnas > lado){
		printf("Los valores filas, columnas y lado son incorrectos\n");
		return;
	}
	
	int * temporal = new int[lado*lado];
	*minimo = INT_MAX;
	*maximo = INT_MIN;
	int valor;
	unsigned long i;
	for(unsigned int p = 0; p < lado; p++){
		for(unsigned int q = 0; q < lado; q++){
			i = p * lado + q;
			if(p < filas && q < columnas){
				fe.read((char *) (& valor), sizeof(int));
				temporal[i] = valor;
				if(valor < *minimo) *minimo = valor;
				if(valor > *maximo) *maximo = valor;
			}else{
				temporal[i] = -1;	// Extension
			}
			
		}
	}
	fe.close();
	
	for(i = 0; i < lado*lado; i++){
		if(temporal[i] != -1){
			rowMajor[i] = temporal[i]-*minimo;
		}else{
			rowMajor[i] = -1;
		}
	}
	delete [] temporal;
	
}


void get_k2raster(const short * rowMajor, short * l_max, short * l_min, bool * l_filter, unsigned long cant_elementos, unsigned long tam, unsigned long level, unsigned int lado){
	short * zorder = new short[cant_elementos];
	short * min = new short[tam];
	short * max = new short[tam];
	bool * filter = new bool[tam];
	
	unsigned long cant_cuarta = cant_elementos >> 2, pos_l_max = 0, pos_l_min = 0, pos_l_filter = 0, base_zorder;
	short e_min, e_max;
	unsigned char j;
	
	/*** construccion z-order ***/
	unsigned int p,q;
	for(unsigned long i = 0; i < cant_elementos; i++){
		p = (unsigned int) (i / lado);
		q = (unsigned int) (i % lado);
		zorder[get_zorder(p, q)] = rowMajor[i];
	}
	
	/*** Primer nivel ***/
	for(unsigned long i = 0; i < cant_cuarta; i++){
		base_zorder = i << 2;
		
		e_min = zorder[base_zorder];
		e_max = zorder[base_zorder];
		
		for(j = 1; j < 4; j++){
			if(zorder[base_zorder+j] < e_min) e_min = zorder[base_zorder+j];
			if(zorder[base_zorder+j] > e_max) e_max = zorder[base_zorder+j];
		}
		min[i] = e_min;
		max[i] = e_max;
		if(e_min == e_max) filter[i] = false;
		else filter[i] = true;
		
		/*** Completación de l_max ***/
		if(filter[i]){
			for(j = 0; j < 4; j++){
				l_max[pos_l_max+j] = e_max-zorder[base_zorder+j];
				
			}
			pos_l_max += 4;
		}
		
	}
	
	/*** Niveles superiores ***/
	
	unsigned long base = cant_cuarta;
	unsigned long base_anterior = 0;
	cant_cuarta = cant_cuarta >> 2;
	
	for(unsigned int l = 1; l <= level; l++){
		for(unsigned long i = 0; i < cant_cuarta; i++){
			base_zorder = base_anterior + (i << 2);
			
			e_min = min[base_zorder];
			e_max = max[base_zorder];

			for(j = 1; j < 4; j++){
				if(min[base_zorder+j] < e_min) e_min = min[base_zorder+j];
				if(max[base_zorder+j] > e_max) e_max = max[base_zorder+j];
			}
			min[base+i] = e_min;
			max[base+i] = e_max;
			
			if(e_min == e_max) filter[base+i] = false;
			else filter[base+i] = true;
			
			/*** Completación de l_max y l_filter ***/
			if(filter[base+i]){
				for(j = 0; j < 4; j++){
					l_max[pos_l_max+j] = e_max-max[base_zorder+j];
					l_filter[pos_l_filter+j] = filter[base_zorder+j];
				}
				pos_l_max += 4;
				pos_l_filter += 4;
			}
			/*** Completación de l_min ***/
			for(j = 0; j < 4; j++){
				if(filter[base_zorder+j]){
					l_min[pos_l_min] = min[base_zorder+j]-e_min;
					pos_l_min++;
					
				}
			}
		}
		
		base_anterior = base;
		base += cant_cuarta;
		cant_cuarta = cant_cuarta>>2;
	}
	
	
	delete [] zorder;
	delete [] min;
	delete [] max;
	delete [] filter;
}

int main(int argc, char ** argv){
	if(argc < 4){
		printf("%s <FILE> <ROWS> <COLS>\n", argv[0]);
		return -1;
	}
	
	char * filename = argv[1];
	unsigned int filas = atoi(argv[2]);
	unsigned int columnas = atoi(argv[3]);
	
	auto start = chrono::high_resolution_clock::now();
	auto finish = chrono::high_resolution_clock::now();
	
	unsigned int lado  = get_lado(filas, columnas);
	unsigned long cant_elementos = lado * lado;
	unsigned int level = ceil(log2(lado));
	unsigned long tam_total = get_tam_piramide(level);
	
	short * rowMajor = new short[cant_elementos];
	int * minimo = new int;
	int * maximo = new int;
	start = chrono::high_resolution_clock::now();
	read_dataset(filename, rowMajor, filas, columnas, lado, minimo, maximo);
	finish = chrono::high_resolution_clock::now();
	long tiempo_lectura = chrono::duration_cast<chrono::milliseconds> (finish - start).count();
	
	short * l_min = new short[tam_total];
	short * l_max = new short[cant_elementos+tam_total];
	bool * l_filter = new bool[tam_total];
	
	start = chrono::high_resolution_clock::now();
	get_k2raster(rowMajor, l_max, l_min, l_filter, cant_elementos, tam_total, level, lado);
	finish = chrono::high_resolution_clock::now();
	long tiempo_ejecucion = chrono::duration_cast<chrono::milliseconds> (finish - start).count();
	
	cout << filename << "\t" << tiempo_lectura << "\t" << tiempo_ejecucion << endl;
	
	delete minimo;
	delete maximo;
	delete [] rowMajor;
	delete [] l_min;
	delete [] l_max;
	delete [] l_filter;
	
	return 0;
}