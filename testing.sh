#! /usr/bin/env bash

TOPE=1000

#tiempo secuencial
for ((E=1;E<=$TOPE;E++));
do
	./encode-secuencial-tiempo 128.bin 128 128
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-secuencial-tiempo 256.bin 256 256
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-secuencial-tiempo 512.bin 512 512
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-secuencial-tiempo 1024.bin 1024 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-secuencial-tiempo 2048.bin 2048 2048
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-secuencial-tiempo 4096.bin 4096 4096
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-secuencial-tiempo 8192.bin 8192 8192
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-secuencial-tiempo eua_join4X4_00.0.bin 14400 14400
done

# tiempo paralelo
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo 128.bin 128 128 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo 256.bin 256 256 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo 512.bin 512 512 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo 1024.bin 1024 1024 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo 2048.bin 2048 2048 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo 4096.bin 4096 4096 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo 8192.bin 8192 8192 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 1024
done

# tiempo lectura z
for ((E=1;E<=$TOPE;E++));
do
	./encode-lecturaz-tiempo 128.bin 128 128 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-lecturaz-tiempo 256.bin 256 256 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-lecturaz-tiempo 512.bin 512 512 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-lecturaz-tiempo 1024.bin 1024 1024 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-lecturaz-tiempo 2048.bin 2048 2048 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-lecturaz-tiempo 4096.bin 4096 4096 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-lecturaz-tiempo 8192.bin 8192 8192 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-lecturaz-tiempo eua_join4X4_00.0.bin 14400 14400 1024
done

# tiempo pinned
for ((E=1;E<=$TOPE;E++));
do
	./encode-pinned-tiempo 128.bin 128 128 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-pinned-tiempo 256.bin 256 256 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-pinned-tiempo 512.bin 512 512 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-pinned-tiempo 1024.bin 1024 1024 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-pinned-tiempo 2048.bin 2048 2048 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-pinned-tiempo 4096.bin 4096 4096 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-pinned-tiempo 8192.bin 8192 8192 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-pinned-tiempo eua_join4X4_00.0.bin 14400 14400 1024
done

# tiempo kernels
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo-kernel 128.bin 128 128 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo-kernel 256.bin 256 256 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo-kernel 512.bin 512 512 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo-kernel 1024.bin 1024 1024 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo-kernel 2048.bin 2048 2048 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo-kernel 4096.bin 4096 4096 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo-kernel 8192.bin 8192 8192 1024
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo-kernel eua_join4X4_00.0.bin 14400 14400 1024
done

# tiempo hebras
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 8
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 16
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 32
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 64
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 128
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 256
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 512
done
for ((E=1;E<=$TOPE;E++));
do
	./encode-paralelo-tiempo eua_join4X4_00.0.bin 14400 14400 1024
done
